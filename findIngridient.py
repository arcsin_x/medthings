"""
Модуль ищет активное вещество для препарата по RXNorm
"""
import time
from typing import List
import requests

# Урлы
url_find_id = "https://rxnav.nlm.nih.gov/REST/rxcui" # Урл для поиска id
url = "https://rxnav.nlm.nih.gov/REST/rxcui/{0}/related" # Урл для поиска ингридиентов
# Сессия
session = requests.session() # Создаем сессию чтобы не писат ькаждый раз хедер
session.headers = {"Accept":"application/json"} # Говорим что хотим жсон

last_time = time.time() # Тк, у нас ограничение в 10 запросов в секунду, будем делать осторожнее чтобы нас не забанили

def stopper(timer:float = 0.34):
    """Декоратор для ограничения времени запросов
    Keyword Arguments:
        timer {float} -- Время на каждый запрос (default: {0.34})
    """
    def in_decorator(func): # Получаем функцию
        def stopped(*args, **kwargs): # И ее аргументы
            # Выполняем код декоратора
            global last_time 
            if time.time() - last_time < timer: # То есть если прошло меньше времени с запроса, ждем
                time.sleep(timer)
            # Выполняем функцию
            ret = func(*args, **kwargs)
            # И снова код декоратора
            last_time = time.time()
            return ret # Не забываем возврат функции
        return stopped
    return in_decorator

@stopper(0.34) # Ставим задержку для 3х запросов в секунду
def get_drug_id(name:str) -> str:
    """ Ищет по базе ид для препарата
    Arguments:
        name {str} -- название препарата
    Returns:
        str -- найденное ид препарата или пустая строка
    """
    r = session.get(url=url_find_id, params={"name":name}).json()["idGroup"]
    #print(r)
    return r["rxnormId"][0] if "rxnormId" in r else "" # Возвращаем код если он есть

@stopper(0.34)
def get_drug_Active(name:str) -> List[str]:
    """ Ищет составляющие препарата по базе rxnorm
    Arguments:
        name {str} -- название препарата
    Returns:
        List[str] -- Список составляющих
    """
    id = get_drug_id(name) # Вызов функции на поиск ид
    if not id: # Если идентификатора не существует
        print("No drug: ", name)
        return [] # Возвращаем пустоту
    r = session.get(url=url.format(id),params={"tty":"IN"}).json()["relatedGroup"]["conceptGroup"][0] # Поиск
    #print(r)
    return [x["name"] for x in r["conceptProperties"]] if "conceptProperties" in r else [] # Возвращаем список веществ если они есть
