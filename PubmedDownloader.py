import os
import time
import requests
from typing import List, Dict, Any

from bs4 import BeautifulSoup

last_time = time.time()  #  Последнее время
TIMER = 0.35  #  Время на остановку
STEP = 900  #  Количество статей за итерацию
STARTYEAR = "2000"  #  Год с которого мы начинаем читать статьи

try:  
    os.mkdir("data")  #  Пробуем создать папку
except:
    pass

def decorator_stop(func):
    """ Декоратор на остановку """
    def fun(*args, **kwargs):
        global last_time
        dtime = time.time() - last_time
        if dtime < TIMER:
            time.sleep(TIMER - dtime)
        r = func(*args, **kwargs)
        last_time = time.time()
        return r
    return fun


@decorator_stop
def get_ids(offset: int=0):
    """ Функция получаящая список ид """
    url_ids = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi'
    text_ids = "has abstract [FILT]"
    args_ids = dict(db="pubmed", retmode="json", retstart=offset, retmax=str(STEP), term=text_ids, sort="pub+date", mindate=STARTYEAR)
    x = requests.get(url_ids, params=args_ids).json()
    return x["esearchresult"]["idlist"]



def get_abstracts(ids: List[str]) -> Dict[str, str]:
    """ Скачивает абстракты для айдишников
    Arguments:
        ids {List[str]} -- Список ид
    
    Returns:
        Dict[str, str] -- Словарь вида {ид:текст}
    """
    @decorator_stop
    def get(ids):
        """ Скачивает абстракты для некоторых ид """
        print("getting:", ids[0], " - ", ids[-1])
        url_texts = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi"
        args_texts = dict(db="pubmed", retmode="xml", rettype="abstract", id=",".join(ids))
        x = requests.get(url_texts, params=args_texts).text

        soup = BeautifulSoup(x)  #  СУП!
        return {x:y.text for x,y in zip(ids,soup.find_all("abstracttext"))}
    
    ret = {}
    offset, LEN = 0, 180
    while offset*LEN < len(ids):
        ret.update(get(ids[offset*LEN:(offset+1)*LEN]))
        offset += 1
    return ret


if __name__ == "__main__":
    #  Проверяем оффсет
    try:
        with open("data/last_id.txt", "r") as f:
            offset = int(f.read())
    except FileNotFoundError:
        offset = 0
    # Открываем файлы
    filetext, fileids = open("data/abstracts.txt", "a"), open("data/abstracts_ids", "a")
    while True:  #  В предполодительно бесконечном цикле скачиваем 
        print("Downloaded: ", STEP*offset)
        while True: # На ошибку цикл
            try:
                ids = get_ids(STEP*offset)
                abst = get_abstracts(ids)
                break
            except Exception as exp:
                print ("Ошибка! ")
                print(exp)
        print("saving")
        txt, ids = "", ""
        for id in abst:
            txt += abst[id]+"\n#END\n"
            ids += id+"\n"
        filetext.write(txt)
        fileids.write(ids)
        
        offset += 1
        with open("data/last_id.txt", "w") as f:
            f.write(str(offset))


